package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import Factory.FileManagerFactory;

public class ClientWindow implements ActionListener {

	private static LinkedList<String> fileContent = new LinkedList<>();

	JFrame frame;
	JTextField jtxInput;
	JTextArea logTxtInput;

	JButton btnSave;
	JButton btnClose;
	JButton btnLoadFile;

	ActionListener saveButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser c = new JFileChooser();
			// Demonstrate "Save" dialog:
			int rVal = c.showSaveDialog(frame);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				jtxInput.setText(c.getCurrentDirectory().toString() + "\\" + c.getSelectedFile().getName());
			}
			if (rVal == JFileChooser.CANCEL_OPTION) {
				jtxInput.setText("File not saved. You pressed cancel");
			}

		}
	};

	ActionListener loadButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser c = new JFileChooser();
			int rVal = c.showOpenDialog(frame);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				jtxInput.setText(c.getCurrentDirectory().toString() + "\\" + c.getSelectedFile().getName());

				try {
					// c.getCurrentDirectory().toString() + "\\" +
					// c.getSelectedFile().getName()
					File file = new File(c.getCurrentDirectory().toString());
					try {
						System.out.println(c.getSelectedFile().getName().split("\\.")[0]);
						fileContent = FileManagerFactory.getInstance().createFileManager()
								.readEnumFile(c.getSelectedFile().getName().split("\\.")[0], file);
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					for (String string : fileContent) {
						logTxtInput.setText(logTxtInput.getText() + "\n" + string);
					}
					logTxtInput.setFont(new Font("sanserif", Font.CENTER_BASELINE, 14));

				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}

			}
			if (rVal == JFileChooser.CANCEL_OPTION) {
				jtxInput.setText("You pressed cancel");
			}

		}
	};

	ActionListener closeButtonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	};

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	public ClientWindow() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				init();
			}
		});
	}

	public void init() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (UnsupportedLookAndFeelException e) {
		}

		// Big frame
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setTitle("Client");
		frame.setSize(800, 600);

		// Some other settings
		final JPanel title = new JPanel(new FlowLayout(FlowLayout.LEFT));
		title.setBackground(new Color(255, 255, 255));
		final JLabel lblAppName = new JLabel("Client Application");
		lblAppName.setFont(new Font("sansserif", Font.PLAIN, 22));
		title.add(lblAppName);
		title.setBorder(BorderFactory.createTitledBorder(""));

		// Adding the upper part of the application
		final JPanel panelLogArea = new JPanel(new GridLayout());
		panelLogArea.setBorder(BorderFactory.createTitledBorder("Input"));
		jtxInput = new JTextField("");
		jtxInput.setEditable(false);
		btnLoadFile = new JButton("Load File");
		btnLoadFile.addActionListener(loadButtonListener);
		panelLogArea.add(btnLoadFile);
		panelLogArea.add(jtxInput);

		// Content of the file
		final JPanel fileLogArea = new JPanel(new GridLayout());
		fileLogArea.setBorder(BorderFactory.createTitledBorder("File Contents"));
		logTxtInput = new JTextArea("");
		logTxtInput.setEditable(false);
		fileLogArea.add(logTxtInput);

		final JPanel panelCommandBoard = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelCommandBoard.setBorder(BorderFactory.createTitledBorder("Client Commands"));

		btnSave = new JButton("Save");
		btnSave.addActionListener(saveButtonListener);

		btnClose = new JButton("Close");
		btnClose.addActionListener(closeButtonListener);

		panelCommandBoard.add(btnSave);
		panelCommandBoard.add(btnClose);

		frame.add(title, BorderLayout.NORTH);
		frame.add(panelCommandBoard, BorderLayout.SOUTH);
		frame.add(panelLogArea, BorderLayout.NORTH);
		frame.add(fileLogArea, BorderLayout.CENTER);

		frame.setVisible(true);
	}

	public static void main(String[] args) {
		ClientWindow m = new ClientWindow();
	}
}
