package Utils;

public class Utils {

	public static String getPropertyType(String propertyName) {

		// if the string contains numbers it might be a number
		if (propertyName.matches("[0-9]+")) {
			return "integer";
		}
		if (propertyName.matches("[a-zA-Z]+")) {
			return "string";
		} else {
			return "unknown type";
		}

		// return propertyName;
	}

	public static String validatePropertyName(String propertyName) {

		return propertyName;

	}
}
