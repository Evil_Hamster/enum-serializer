package Utils;

public enum IdsEnum {

	NORTH("1"), NORTHEAST("2"), EAST("3"), SOUTHEAST("4"), SOUTH("5"), SOUTHWEST("6"), WEST("7"), NORTHWEST("8"), PUKA(
			"203"), NORTHEfAST(
					"2"), NORTHEgAST("2"), NORTHEAStT("2"), NORTHjEAST("2"), NORTaHEAST("2"), NORTHtEAST("2");

	private String direction;

	private IdsEnum(String direction) {
		this.direction = direction;
	}

	public String getDirection() {
		return direction;
	}

	public String toString() {
		return this.name() + "=" + this.direction;
	}
}
