package Interfaces;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;

public interface ReadEnumInterface {

	LinkedList<String> readEnumFile(String enumName, File fileToBeLoaded)
			throws ClassNotFoundException, MalformedURLException;

	void savePropertyFile(LinkedList<String> fileContent, String fileToBeWritten) throws IOException;

	String printFile(String fileName) throws IOException;

}
