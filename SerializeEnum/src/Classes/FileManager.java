package Classes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;

import Interfaces.ReadEnumInterface;

public class FileManager implements ReadEnumInterface {

	private File file;
	private FileOutputStream fop = null;

	/**
	 * This method will be loading a properties file and returning a LinkedList
	 * containing values inside the file.
	 * 
	 * @param fileName
	 *            Name of the file to be loaded
	 * @throws IOException
	 *             In case of damaged file or file not existing, IOException
	 *             will be thrown.
	 * @return A HashmMap containing values of the properties file.
	 * @throws ClassNotFoundException
	 *             If the name of the class is incorrect.
	 * @throws MalformedURLException
	 */
	@Override
	public LinkedList<String> readEnumFile(String enumName, File fileToBeLoaded)
			throws ClassNotFoundException, MalformedURLException {

		LinkedList<String> returnValues = new LinkedList<>();

		URL[] cp = { fileToBeLoaded.toURI().toURL() };
		URLClassLoader urlcl = new URLClassLoader(cp);
		Class<?> c = urlcl.loadClass(enumName);

		// Class<?> c = (Class.forName(enumName));
		Object[] map = null;

		try {
			try {
				map = (Object[]) c.getDeclaredMethod("values").invoke(c, new Object[] {});
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				System.out.println("Error when trying to get the enum values.");
				e.printStackTrace();
			}
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

		for (Object id : map) {
			returnValues.add(id.toString());
		}

		return returnValues;
	}

	/**
	 * This method will be writing content of a previously loaded file to an
	 * another file.
	 * 
	 * @param fileContent
	 *            A hash map returned by the readFile method.
	 * @param fileToBeWritten
	 *            The file where content is going to be written.
	 * @throws IOException
	 *             In case of damaged file or file not existing, IOException
	 *             will be thrown.
	 */
	@Override
	public void savePropertyFile(LinkedList<String> fileContent, String fileToBeWritten) throws IOException {

		file = new File(fileToBeWritten);
		fop = new FileOutputStream(file);
		String content = "";

		if (!file.exists()) {
			file.createNewFile();
		}
		if (fileContent != null) {

			for (String e : fileContent) {
				content += e + "\n";
			}
			byte[] bytesContent = content.getBytes();
			fop.write(bytesContent);
			fop.flush();
			fop.close();

			System.out.println("Done writing to file - " + fileToBeWritten);
		} else {
			System.out.println("There is no file loaded for writing, please load a file before trying to  write it");
		}
	}

	@Override
	public String printFile(String fileName) throws IOException {
		return fileName;

	}
}
