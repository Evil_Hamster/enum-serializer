package Factory;

import Classes.FileManager;

public class FileManagerFactory {

	private static FileManagerFactory fileReader;

	public static FileManagerFactory getInstance() {
		if (fileReader == null) {
			fileReader = new FileManagerFactory();
		}

		return fileReader;
	}

	public FileManager createFileManager() {
		return new FileManager();
	}

	private FileManagerFactory() {
		// implicit constructor
	}
}
